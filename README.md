# React Podcast

Web-app for searching and playing podcasts. 

## Add .env 

REACT_APP_API_URL = https://itunes.apple.com

## Available Scripts

In the project directory, you can run:

### `npm install`
### `npm start`

or

### `yarn`
### `yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

[GitLab](https://gitlab.com/alejandrofarneda)

[LinkedIn](https://www.linkedin.com/in/alejandro-farneda/)
