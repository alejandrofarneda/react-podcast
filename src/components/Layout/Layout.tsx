import { Header } from "../Header";
import "./Layout.scss";

export function Layout({ children }: { children: JSX.Element }) {
  return (
    <>
      <Header />
      <div className="layout-body">{children}</div>
    </>
  );
}
