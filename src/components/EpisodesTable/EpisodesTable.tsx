import { Link } from "react-router-dom";
import { map } from "lodash";
import { EpisodesTableProps } from "./EpisodesTable.types";
import "./EpisodesTable.scss";

export function EpisodesTable(props: EpisodesTableProps) {
  const { episodes, podcastId } = props;

  return (
    <div className="episodes-table-container">
      <table className="episodes-table">
        <tbody>
          <tr>
            <th>Title</th>
            <th>Date</th>
            <th>Duration</th>
          </tr>

          {map(episodes, (episode) => (
            <tr key={episode.id}>
              <td>
                <Link
                  style={{
                    textDecoration: "none",
                    color: "#14709e",
                  }}
                  to={`/podcast/${podcastId}/episode/${episode.id}`}
                >
                  {episode.name}
                </Link>
              </td>
              <td>{episode.releaseData}</td>
              <td>{episode.trackTime}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
