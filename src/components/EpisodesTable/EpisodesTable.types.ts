import { EpisodeDomainModel } from "../../models";

export type EpisodesTableProps = {
  podcastId: string;
  episodes: EpisodeDomainModel[];
};
