import { Link } from "react-router-dom";
import "./Header.scss";
import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";

export function Header() {
  const location = useLocation();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);

    const timer = setTimeout(() => {
      setIsLoading(false);
    }, 1000);

    return () => clearTimeout(timer);
  }, [location]);

  return (
    <div className="header">
      <Link to="/">
        <h2>Podcaster</h2>
      </Link>

      <div className={isLoading ? "pulsating-dot" : "simple-dot"} />
    </div>
  );
}
