import { SearchPodcastProps } from "./SearchPodcast.types";
import "./SearchPodcast.scss";

export function SearchPodcast(props: SearchPodcastProps) {
  const { podcastCount, onChange } = props;

  return (
    <div className="home-input-container">
      <div className="home-input-inner">
        <span>{podcastCount}</span>
        <input
          className="home-input"
          placeholder="Search by Title or Artist..."
          onChange={onChange}
        />
      </div>
    </div>
  );
}
