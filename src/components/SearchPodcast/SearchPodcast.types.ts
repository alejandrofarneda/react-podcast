export type SearchPodcastProps = {
  podcastCount: string | number;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
};
