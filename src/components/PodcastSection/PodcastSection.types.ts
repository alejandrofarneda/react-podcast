export type PodcastSectionProps = {
  image: string;
  title: string;
  author: string;
  description: string;
};
