import { PodcastSectionProps } from "./PodcastSection.types";
import "./PodcastSection.scss";

export function PodcastSection(props: PodcastSectionProps) {
  const { image, title, author, description } = props;

  return (
    <div className="podcast-section">
      <img src={image} alt={title} />
      <div className="podcast-separator" />
      <h3>{title}</h3>
      <span>by {author}</span>
      <br />
      <div className="podcast-separator" />
      <br />
      <span>Description:</span>
      <br />
      <span>{description}</span>
    </div>
  );
}
