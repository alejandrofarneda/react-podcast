import { Link } from "react-router-dom";
import { PodcastCardProps } from "./PodcastCard.types";
import "./PodcastCard.scss";

export function PodcastCard(props: PodcastCardProps) {
  const { id, title, author, image } = props;

  return (
    <div className="podcast-card">
      <Link to={`/podcast/${id}`}>
        <img src={image} alt={title} />
        <h2>{title}</h2>
        <span>Author: {author}</span>
      </Link>
    </div>
  );
}
