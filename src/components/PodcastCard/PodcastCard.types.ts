export type PodcastCardProps = {
  id: string;
  title: string;
  author: string;
  image: string;
};
