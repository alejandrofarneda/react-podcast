import { map } from "lodash";
import { EpisodeApiModel, EpisodeDomainModel } from "../models";
import { dateParser } from "./dateParser";
import { msToTime } from "./msToTime";

export const episodesToDomain = (data: {
  results: EpisodeApiModel[];
  resultCount: number;
}): EpisodeDomainModel[] => {
  const parsedData = map(data.results, (item) => {
    return {
      id: item.trackId.toString(),
      name: item.trackName,
      description: item.description,
      releaseData: dateParser(item.releaseDate),
      trackTime: msToTime(item.trackTimeMillis),
      imageUrl: item.previewUrl,
      totalEpisodes: data.resultCount.toString(),
    };
  });

  return parsedData;
};
