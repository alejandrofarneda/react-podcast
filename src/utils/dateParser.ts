export function dateParser(date: string) {
  var result = date.split("-");

  var year = result[0];
  var month = result[1];
  var day = result[2].slice(0, 2);

  return `${day}/${month}/${year}`;
}
