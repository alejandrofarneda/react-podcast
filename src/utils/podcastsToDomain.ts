import { map } from "lodash";
import { PodcastApiModel, PodcastDomainModel } from "../models";

export const podcastsToDomain = (
  data: PodcastApiModel[]
): PodcastDomainModel[] => {
  const parsedData = map(data, (item) => {
    const images = {
      small: {
        uri: item["im:image"][1].label,
        height: item["im:image"][1].attributes.height,
      },
      medium: {
        uri: item["im:image"][2].label,
        height: item["im:image"][2].attributes.height,
      },
    };

    return {
      id: item.id.attributes["im:id"],
      name: item["im:name"].label,
      artist: item["im:artist"].label,
      images,
      summary: item.summary.label,
      title: item.title.label,
      category: item.category.attributes.label,
      link: item.link.href,
    };
  });

  return parsedData;
};
