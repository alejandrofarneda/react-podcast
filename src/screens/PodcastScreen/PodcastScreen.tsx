import { useEffect, useState } from "react";
import { EpisodesTable, Layout, PodcastSection } from "../../components";
import { EpisodeDomainModel, PodcastDomainModel } from "../../models";
import { useParams } from "react-router-dom";
import "./PodcastScreen.scss";
import { getData } from "../../server";

const { getPodcast, getEpisodes } = getData();

export function PodcastScreen() {
  const { id = "" } = useParams();
  const [podcast, setPodcast] = useState<PodcastDomainModel>();
  const [episodes, setEpisodes] = useState<EpisodeDomainModel[]>();

  useEffect(() => {
    (async () => {
      try {
        const podcast = await getPodcast(id);
        setPodcast(podcast);
      } catch (error) {
        console.error(error);
      }
    })();
  }, [getPodcast, id]);

  useEffect(() => {
    (async () => {
      try {
        const response = await getEpisodes(id);
        setEpisodes(response);
      } catch (error) {
        throw error;
      }
    })();
  }, [getEpisodes, id]);

  if (!episodes || !podcast) return null;

  return (
    <Layout>
      <div className="podcast-screen-container">
        <PodcastSection
          author={podcast.artist}
          description={podcast.summary}
          image={podcast.images.medium.uri}
          title={podcast.name}
        />
        <div className="total-table-container">
          <div className="total-episodes">
            <h3>Episodes: {episodes[0].totalEpisodes}</h3>
          </div>
          <EpisodesTable episodes={episodes} podcastId={id} />
        </div>
      </div>
    </Layout>
  );
}
