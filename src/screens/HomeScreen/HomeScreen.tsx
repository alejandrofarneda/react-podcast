import { size } from "lodash";
import { useEffect, useState } from "react";
import { Layout, PodcastCard, SearchPodcast } from "../../components";
import { getData } from "../../server";
import { PodcastDomainModel } from "../../models";
import "./HomeScreen.scss";

const { getPodcasts } = getData();

export function HomeScreen() {
  const [podcasts, setPodcasts] = useState<PodcastDomainModel[]>();
  const [query, setQuery] = useState("");
  const filterPodcast = podcasts?.filter((podcast) => {
    if (query === "") {
      return podcast;
    } else if (
      podcast.title.toLowerCase().includes(query.toLowerCase()) ||
      podcast.artist.toLocaleLowerCase().includes(query.toLowerCase())
    ) {
      return podcast;
    }
  });

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    setQuery(event.target.value);

  useEffect(() => {
    (async () => {
      try {
        const podcasts = await getPodcasts();

        setPodcasts(podcasts);
      } catch (error) {
        console.error(error);
      }
    })();
  }, [getPodcasts]);

  if (!podcasts) return null;

  return (
    <Layout>
      <>
        <SearchPodcast
          onChange={onChange}
          podcastCount={size(filterPodcast) || "0"}
        />
        <section className="home-body">
          {filterPodcast?.map((podcast, index) => (
            <PodcastCard
              id={podcast.id}
              author={podcast.artist}
              image={podcast.images.medium.uri}
              title={podcast.name}
              key={index}
            />
          ))}
        </section>
      </>
    </Layout>
  );
}
