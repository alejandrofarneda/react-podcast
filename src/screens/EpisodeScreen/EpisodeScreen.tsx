import { useEffect, useState } from "react";
import { EpisodeDomainModel, PodcastDomainModel } from "../../models";
import { useParams } from "react-router-dom";
import { Layout, PodcastSection } from "../../components";
import "./EpisodeScreen.scss";
import { getData } from "../../server";

const { getEpisode, getPodcast } = getData();

export function EpisodeScreen() {
  const { podcastId = "", episodeId = "" } = useParams();
  const [podcast, setPodcast] = useState<PodcastDomainModel>();
  const [episode, setEpisode] = useState<EpisodeDomainModel>();

  useEffect(() => {
    (async () => {
      try {
        const podcast = await getPodcast(podcastId);
        setPodcast(podcast);
      } catch (error) {
        console.error(error);
      }
    })();
  }, [getPodcast, podcastId]);

  useEffect(() => {
    (async () => {
      try {
        const episode = await getEpisode(podcastId, episodeId);
        setEpisode(episode);
      } catch (error) {
        console.error(error);
      }
    })();
  }, [getEpisode, episodeId, podcastId]);

  if (!episode || !podcast) return null;

  return (
    <Layout>
      <div className="episode-screen">
        <PodcastSection
          author={podcast.artist}
          description={podcast.summary}
          image={podcast.images.medium.uri}
          title={podcast.name}
        />
        <div className="episode-screen-description">
          <h2>{episode.name}</h2>
          <p
            dangerouslySetInnerHTML={{
              __html: episode.description || "No description...",
            }}
          />
          <audio src={episode.imageUrl} controls />
        </div>
      </div>
    </Layout>
  );
}
