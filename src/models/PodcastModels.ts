type ImageApiModel = {
  label: string;
  attributes: { height: string };
};

export type PodcastApiModel = {
  id: {
    attributes: {
      "im:id": string;
    };
  };
  "im:name": {
    label: string;
  };
  "im:artist": {
    label: string;
  };
  "im:image": ImageApiModel[];
  summary: {
    label: string;
  };
  title: {
    label: string;
  };
  category: {
    attributes: {
      label: string;
    };
  };
  link: { href: string };
};

export type PodcastDomainModel = {
  id: string;
  name: string;
  artist: string;
  images: {
    small: {
      uri: string;
      height: string;
    };
    medium: {
      uri: string;
      height: string;
    };
  };
  summary: string;
  title: string;
  category: string;
  link: string;
};
