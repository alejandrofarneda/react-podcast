export type EpisodeApiModel = {
  trackId: number;
  trackName: string;
  releaseDate: string;
  trackTimeMillis: number;
  previewUrl: string;
  description: string;
};

export type EpisodeDomainModel = {
  id: string;
  name: string;
  description: string;
  releaseData: string;
  trackTime: string;
  imageUrl: string;
  totalEpisodes: string;
};
