import { HomeScreen, PodcastScreen, EpisodeScreen } from "../screens";
import { createBrowserRouter } from "react-router-dom";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <HomeScreen />,
  },
  {
    path: "/podcast/:id",
    element: <PodcastScreen />,
  },
  {
    path: "/podcast/:podcastId/episode/:episodeId",
    element: <EpisodeScreen />,
  },
]);
