import { storage } from "./storage";
import { EpisodeDomainModel, PodcastDomainModel } from "../models";
import { episodesToDomain, podcastsToDomain } from "../utils";

const {
  getStoragePodcasts,
  saveStoragePodcasts,
  saveStorageEpisodes,
  getStorageEpisodes,
} = storage();

export const getData = () => {
  const apiUrl = process.env.REACT_APP_API_URL;

  async function getPodcasts(): Promise<PodcastDomainModel[] | undefined> {
    try {
      const localPodcasts = getStoragePodcasts();

      if (localPodcasts) {
        return localPodcasts;
      }

      const response = await fetch(
        `${apiUrl}/us/rss/toppodcasts/limit=100/genre=1310/json`
      );

      const result = await response.json();
      const parsedPodcasts = podcastsToDomain(result.feed.entry);

      saveStoragePodcasts(parsedPodcasts);

      return parsedPodcasts;
    } catch (error) {
      console.error(error);
    }
  }

  async function getPodcast(podcastId: string) {
    try {
      const podcasts = await getPodcasts();

      if (!podcasts) throw new Error("Unexpected error returning data");

      const podcast = podcasts?.find((item) => {
        return item.id === podcastId;
      });

      if (!podcast) throw new Error("Unexpected error returning data");

      return podcast;
    } catch (error) {
      throw error;
    }
  }

  async function getEpisodes(
    podcastId: string
  ): Promise<EpisodeDomainModel[] | undefined> {
    try {
      const localEpisodes = getStorageEpisodes(podcastId);

      if (localEpisodes) {
        return localEpisodes;
      }

      const url = `${apiUrl}/lookup?id=${podcastId}&entity=podcastEpisode`;
      const response = await fetch(url);
      const result = await response.json();
      const parsedEpisodes = episodesToDomain(result);

      saveStorageEpisodes(podcastId, parsedEpisodes);

      return parsedEpisodes;
    } catch (error) {
      throw error;
    }
  }

  async function getEpisode(podcastId: string, episodeId: string) {
    try {
      const episodes = await getEpisodes(podcastId);
      const episode = episodes?.find((item) => {
        return item.id === episodeId;
      });

      if (!episode) throw new Error("Unexpected error returning data");

      return episode;
    } catch (error) {
      throw error;
    }
  }

  return {
    getPodcasts,
    getPodcast,
    getEpisode,
    getEpisodes,
  };
};
