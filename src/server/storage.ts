import { EpisodeDomainModel, PodcastDomainModel } from "../models";

export const storage = () => {
  const expireTime = 24;
  const podcastsKey = "podcasts";

  function saveStoragePodcasts(podcasts: PodcastDomainModel[]) {
    const expiresIn = new Date();
    expiresIn.setHours(expiresIn.getHours() + expireTime);

    const item = {
      value: podcasts,
      expiry: expiresIn,
    };

    localStorage.setItem(podcastsKey, JSON.stringify(item));
  }

  function getStoragePodcasts() {
    const data = localStorage.getItem(podcastsKey);

    if (!data) {
      return null;
    }

    const dataJson = JSON.parse(data);
    const now = new Date();

    if (now.getTime() > dataJson.expiry) {
      localStorage.removeItem(podcastsKey);
      return null;
    }

    return dataJson.value;
  }

  function saveStorageEpisodes(
    idPodcast: string,
    episodes: EpisodeDomainModel[]
  ) {
    const expiresIn = new Date();
    expiresIn.setHours(expiresIn.getHours() + expireTime);

    const item = {
      value: episodes,
      expiry: expiresIn,
    };

    localStorage.setItem(idPodcast, JSON.stringify(item));
  }

  function getStorageEpisodes(podcastId: string) {
    const data = localStorage.getItem(podcastId);

    if (!data) {
      return null;
    }

    const dataJson = JSON.parse(data);
    const now = new Date();

    if (now.getTime() > dataJson.expiry) {
      localStorage.removeItem(podcastId);
      return null;
    }

    return dataJson.value;
  }

  return {
    saveStoragePodcasts,
    getStoragePodcasts,
    saveStorageEpisodes,
    getStorageEpisodes,
  };
};
